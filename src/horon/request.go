package horon

import (
    "bytes"
    "context"
    "net/http"
    "path"
    "strings"

    "pwf/src/common/enc"
)

/* ----- */

type RequestOpt struct {
    Method, URL string
    Param       map[string]interface{}
    Body        interface{}
}

type Response http.Response

/* ----- */

func CurrentPath() string {
    return strings.TrimPrefix(LocationPath(), "/"+DocumentLang())
}

func HostPath(url string) string {
    return path.Join("/", DocumentLang(), url)
}

func Request(opt *RequestOpt) (*Response, error) {
    cbor, err := enc.ToCBOR(opt.Body)
    if err != nil {
        return nil, err
    }

    ctx, body := context.Background(), bytes.NewReader(cbor)
    req, err := http.NewRequestWithContext(ctx, opt.Method, opt.URL, body)
    if err != nil {
        return nil, err
    }

    clt := http.Client{}
    ret, err := clt.Do(req)
    if err != nil {
        return nil, err
    }

    return (*Response)(ret), nil
}

// func RequestCb(opt *RequestOpt, fn func(Response, error)) error {
//     go func() {
//     }()
//     go func() {
//     }()
//     return nil
// }
