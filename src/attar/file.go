package attar

import (
    "io"
    "net/http"
)

/* ----- */

func CatFile(w http.ResponseWriter, f http.File) error {
    _, err := io.Copy(w, f)
    return err
}
