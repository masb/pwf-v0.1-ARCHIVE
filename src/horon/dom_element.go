package horon

import ()

/* ----- */

type DOMElement struct {
    Value

    CbSet []*CallbackSetter
    Hash  uint32
}

/* ----- */

func IsDOMElement(e Element) bool {
    _, ok := e.(*DOMElement)
    return ok
}

func (e *DOMElement) JSValue() Value {
    return e.Value
}

func (e *DOMElement) HashValue() uint32 {
    return e.Hash
}

func (e *DOMElement) Free() {
    for _, i := range e.CbSet {
        i.Cb.Release()
    }
}

func (e *DOMElement) Tag() string {
    return e.Get(ElementTagKey).String()
}

func (e *DOMElement) Text() string {
    return e.Get(ElementTextKey).String()
}

func (e *DOMElement) AttrSet() []Attribute {
    return nil
}

func (e *DOMElement) CbKeySet() []string {
    ret := make([]string, len(e.CbSet))
    for n, i := range e.CbSet {
        ret[n] = i.Key
    }
    return ret
}

func (e *DOMElement) Children() []Element {
    return nil
}
