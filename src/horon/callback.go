package horon

import (
    "syscall/js"
)

/* ----- */

type Callback struct {
    js.Func
}

type CallbackFunc = func(Value, []Value) interface{}

/* ----- */

func NewCallback(fn CallbackFunc) Callback {
    return Callback{Func: js.FuncOf(fn)}
}

func NewWrappedCallback(fn func()) Callback {
    return NewCallback(func(Value, []Value) interface{} { fn(); return nil })
}

func (cb Callback) ListenAll(v Value) {
    for _, i := range [...]string{OnClickKey} {
        v.Set(i, cb)
    }
}
