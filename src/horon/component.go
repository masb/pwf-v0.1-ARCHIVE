package horon

import ()

/* ----- */

type Component interface {
    View() Element

    Set(Element) Element
    Get() Element
}

type ComponentWithInit interface {
    Component

    OnInit()
}

type ComponentCache struct{ Element }

/* ----- */

func (c *ComponentCache) Set(e Element) Element {
    c.Element = e
    return e
}

func (c *ComponentCache) Get() Element {
    return c.Element
}
