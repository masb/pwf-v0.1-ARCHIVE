package sugar

import (
    "sync"
)

/* ----- */

type ChanIter struct {
    C chan error
    M sync.Mutex
}

/* ----- */

func (i *ChanIter) Begin() chan error {
    return i.C
}

func (i *ChanIter) Next() {
    i.M.Unlock()
}
