package main

import (
    "fmt"
    "net/http"
    "os"

    "pwf/src/attar"
    "pwf/src/common/enc"
)

/* ----- */

type staticHandler struct {
    http.Dir `cbor:"StaticRootDirectory"`
}

/* ----- */

func (h *staticHandler) Init(rc enc.CBOR) (attar.HandlerPath, error) {
    return attar.HandlerPath{Path: "/"}, rc.Unmarshal(h)
}

func (h *staticHandler) Serve(w http.ResponseWriter, r *http.Request) {
    err := h.catFile(w, r.URL.Path)
    if err != nil {
        if os.IsNotExist(err) {
            attar.NotFoundError(w, r.URL.Path)
        } else {
            attar.InternalError(w, err.Error())
        }
    }
}

func (h *staticHandler) catFile(w http.ResponseWriter, path string) error {
    f, err := h.Open(path)
    if err != nil {
        return err
    }

    stat, err := f.Stat()
    if err != nil {
        return err
    }

    if stat.IsDir() {
        return h.lsDir(w, f, path)
    }

    attar.ContentTypeAuto(w, string(h.Dir)+path)
    return attar.CatFile(w, f)
}

func (h *staticHandler) lsDir(w http.ResponseWriter, f http.File, path string) error {
    ls, err := f.Readdir(-1)
    if err != nil {
        return err
    }

    fmt.Fprintf(w, "Listing content of %s:\n\n", path)
    for _, i := range ls {
        fmt.Fprintf(w, "\t%s\n", i.Name())
    }
    return nil
}
